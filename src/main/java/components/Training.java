package components;

import components.enums.InputType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Créé par gabriel le 13/02/18.
 */
public class Training {

    private static double RATE = 0.5d;

    private NeuralNetwork neuralNetwork;

    public Training(NeuralNetwork neuralNetwork) {
        this.neuralNetwork = neuralNetwork;
    }

    private int getError(Neuron neuron, InputType inputType, int index) {
        return inputType.getExpectedValues()[index] - neuron.activate(inputType);
    }

    public void launch() {
        ArrayList<Neuron> neuronsList = neuralNetwork.getNeuronsList();
        boolean isInError;
        printNumberBytes("Initial values");
        do {
            isInError = false;
            // For each neuron
            for(int i = 0; i < neuronsList.size(); i++) {
                Neuron neuron = neuronsList.get(i);
                // For each number
                for(Map.Entry<InputType, ArrayList<NeuronInputUnit>> entry : neuron.getInputs().entrySet()) {
                    // Error calculation
                    int errorDelta = getError(neuron, entry.getKey(), i);
                    if(errorDelta != 0) {
                        isInError = true;
                        // Initialiazing weights variations list
                        for(NeuronInputUnit neuronInputUnit : entry.getValue()) {
                            double currentWeightVariation = RATE * errorDelta * neuronInputUnit.getValue();
                            HashMap<InputType, Double> weightsVariationsList = neuron.getWeightVariationsList();
                            if(!weightsVariationsList.containsKey(entry.getKey())) {
                                weightsVariationsList.put(entry.getKey(), currentWeightVariation);
                            } else {
                                double weight = weightsVariationsList.get(entry.getKey());
                                weightsVariationsList.put(entry.getKey(), weight + currentWeightVariation);
                            }
                        }
                        // Updating weights
                        /*System.out.println(String.format("Updating weights for neuron %s / %s using a %s variation",
                                i + 1, entry.getKey(), neuron.getWeightVariationsList().get(entry.getKey())));*/
                        for(NeuronInputUnit neuronInputUnit : entry.getValue()) {
                            neuronInputUnit.setWeight(neuronInputUnit.getWeight() + neuron.getWeightVariationsList().get(entry.getKey()));
                        }
                    }
                }
            }
            neuralNetwork.incrementIterationsNumber();
        } while(isInError);
        printNumberBytes("Results");
        System.out.println(String.format("\nDone in %s iterations", neuralNetwork.getIterations()));
    }

    private void printNumberBytes(String title) {
        System.out.println(String.format("\n*** %s ***", title));
        for(InputType inputType : InputType.values()) {
            StringBuilder sb = new StringBuilder().append(inputType.toString()).append(" ");
            for(Neuron neuron : neuralNetwork.getNeuronsList()) {
                sb.append(neuron.activate(inputType));
            }
            System.out.println(sb.toString());
        }
    }
}
