package components;

import components.enums.InputType;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Créé par gabriel le 13/02/18.
 */
public class NeuralNetwork {

    private static int MIN_INIT_WEIGHT_THRESHOLD = -10000000;
    private static int MAX_INIT_WEIGHT_THRESHOLD = 10000000;

    @Getter
    private ArrayList<Neuron> neuronsList = new ArrayList<>();

    @Getter
    private int iterations = 1;

    public NeuralNetwork(int neuronsNumber) {
        for(int i = 0; i < neuronsNumber; i++) {
            neuronsList.add(neuronInitialization(new Neuron()));
        }
    }

    public void incrementIterationsNumber() {
        iterations += 1;
    }

    private Neuron neuronInitialization(Neuron neuron) {
        HashMap<InputType, ArrayList<NeuronInputUnit>> neuronInputsList = neuron.getInputs();
        for(InputType inputType : InputType.values()) {
            ArrayList<NeuronInputUnit> neuronInputUnitsList = new ArrayList<>();
            for(int inputUnitValue : inputType.getInputsList()) {
                // Initializing every "input unit" with a random weight between the previously defined thresholds
                neuronInputUnitsList.add(new NeuronInputUnit(inputUnitValue, ThreadLocalRandom.current().nextInt(MIN_INIT_WEIGHT_THRESHOLD, MAX_INIT_WEIGHT_THRESHOLD)));
            }
            neuronInputsList.put(inputType, neuronInputUnitsList);
        }
        return neuron;
    }
}
