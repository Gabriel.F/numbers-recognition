package components;

import components.enums.InputType;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Créé par gabriel le 13/02/18.
 */
@Getter
@NoArgsConstructor
public class Neuron {

    private HashMap<InputType, ArrayList<NeuronInputUnit>> inputs = new HashMap<>();

    @Getter
    private HashMap<InputType, Double> weightVariationsList = new HashMap<>();

    public double agregate(InputType inputType) {
        return getInputs().get(inputType)
                .stream()
                .mapToDouble(n -> n.getValue() * n.getWeight())
                .sum();
    }

    public int activate(InputType inputType) {
        return agregate(inputType) >= 0 ? 1 : 0;
    }
}
