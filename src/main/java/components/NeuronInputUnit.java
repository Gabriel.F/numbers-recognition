package components;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Créé par gabriel le 13/02/18.
 */
@Getter
@AllArgsConstructor
public class NeuronInputUnit {

    private int value;

    @Setter
    private double weight;
}
