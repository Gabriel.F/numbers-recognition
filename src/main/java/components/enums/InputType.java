package components.enums;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;

/**
 * Créé par gabriel le 13/02/18.
 */
@Getter
@AllArgsConstructor
public enum InputType {

    ZERO(Lists.newArrayList(
            1, 1, 1,
            1, 0, 1,
            1, 0, 1,
            1, 0, 1,
            1, 1, 1), new int[] {0, 0, 0}),
    ONE(Lists.newArrayList(
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0,
            0, 1, 0), new int[] {0, 0, 1}),
    TWO(Lists.newArrayList(
            1, 1, 1,
            0, 0, 1,
            1, 1, 1,
            1, 0, 0,
            1, 1, 1), new int[] {0, 1, 0}),
    THREE(Lists.newArrayList(
            1, 1, 1,
            0, 0, 1,
            1, 1, 1,
            0, 0, 1,
            1, 1, 1), new int[] {0, 1, 1}),
    FOUR(Lists.newArrayList(
            1, 0, 1,
            1, 0, 1,
            1, 1, 1,
            0, 0, 1,
            0, 0, 1), new int[] {1, 0, 0}),
    FIVE(Lists.newArrayList(
            1, 1, 1,
            1, 0, 0,
            1, 1, 1,
            0, 0, 1,
            1, 1, 1), new int[] {1, 0, 1}),
    SIX(Lists.newArrayList(
            1, 1, 1,
            1, 0, 0,
            1, 1, 1,
            1, 0, 1,
            1, 1, 1), new int[] {1, 1, 0}),
    SEVEN(Lists.newArrayList(
            1, 1, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1), new int[] {1, 1, 1});


    private ArrayList<Integer> inputsList;
    private int[] expectedValues;
}
