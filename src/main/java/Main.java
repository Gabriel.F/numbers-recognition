import components.NeuralNetwork;
import components.Training;

/**
 * Créé par gabriel le 13/02/18.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("\n****** \n*** Basic neural networking *** \n******\n");

        Training training =  new Training(new NeuralNetwork(3));
        training.launch();
    }
}
